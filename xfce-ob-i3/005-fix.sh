#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install OpenBox Core Applications
sudo pacman -S --needed --noconfirm obconf i3-gaps dmenu
sudo pacman -S --needed --noconfirm i3lock
sudo pacman -S --needed --noconfirm openbox tint2 nitrogen compton 
sudo pacman -S --needed --noconfirm volumeicon lxappearance-obconf
sudo pacman -S --needed --noconfirm perl-data-dump gtk2-perl i3status
sudo pacman -S --needed --noconfirm perl-file-desktopentry

# Install OpenBox Extras
trizen -S --noconfirm obmenu
trizen -S --noconfirm obmenu-generator
trizen -S --noconfirm obbrowser
trizen -S --noconfirm perl-linux-desktopfiles
trizen -S --noconfirm comptray
trizen -S --noconfirm gksu
trizen -S --noconfirm oblogout


# Change oblogout theme
sudo tar xzf tarballs/oblogout-conf.tar.gz -C /etc --overwrite
