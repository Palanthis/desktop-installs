#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis 
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo pacman -S --needed --noconfirm autoconf-archive

git clone https://github.com/intel/thermal_daemon

cd thermal_daemon

sh ./autogen.sh

sh ./configure prefix=/

make

sudo make install

sudo systemctl enable thermald.service

sudo systemctl start thermald.service
