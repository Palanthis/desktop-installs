#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install VirtualBox and optional components
sudo pacman -S --needed --noconfirm virtualbox
sudo pacman -S --needed --noconfirm virtualbox-host-dkms
sudo pacman -S --needed --noconfirm virtualbox-guest-iso

# Virtualbox Extensions
trizen -S --noconfirm --needed virtualbox-ext-oracle
