#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################


# Install VirtualBox and optional components
sudo pacman -S virtualbox --noconfirm --needed
sudo pacman -S virtualbox-host-dkms --noconfirm --needed
sudo pacman -S virtualbox-guest-iso --noconfirm --needed
yay -S virtualbox-ext-oracle --noconfirm --needed
