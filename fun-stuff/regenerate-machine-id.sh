#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "This must be run as root"

rm -rf /etc/machine-id

systemd-machine-id-setup

echo "Machine ID regenreated. Please reboot the system."
