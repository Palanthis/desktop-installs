#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

yay -S --needed --noconfirm zramd

#sudo nano /etc/default/zramd

sudo systemctl enable --now zramd.service
