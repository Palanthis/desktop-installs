#!/bin/bash
# Written by Palanthis
set -e
 
trizen -Sy --needed --noconfirm macfanctld

sudo systemctl enable macfanctld.service

sudo systemctl start macfanctld.service
