#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################


sudo pacman -S --needed --noconfirm openvpn dialog python-pip python-setuptools

git clone https://github.com/protonvpn/linux-cli

cd linux-cli

sudo pip3 install -e .

echo " "
echo "ProtonVPN (CLI) installed."
read -n 1 -s -r -p "Press Enter to install GUI or Ctrl+C to quit."

sudo pacman -S --needed --noconfirm python-gobject gtk3 libappindicator-gtk3 libnotify

git clone https://github.com/calexandru2018/protonvpn-linux-gui

cd protonvpn-linux-gui

sudo pip3 install protonvpn-gui
