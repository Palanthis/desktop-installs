#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

#Install IPTables first (to avoid being prompted
sudo pacman -S --needed --noconfirm iptables-nft

# Install all needed packages for full QEMU/KVM support
sudo pacman -S --needed --noconfirm dnsmasq vde2 virt-manager
sudo pacman -S --needed --noconfirm qemu-desktop libvirt edk2-ovmf qemu-emulators-full
sudo pacman -S --needed --noconfirm qemu-block-iscsi qemu-guest-agent

# Enable libnvirt daemon
sudo systemctl enable libvirtd.service



