#!/bin/bash
# Written by Palanthis
set -e

# Install zsh and optional fonts
sudo pacman -S --needed --noconfirm zsh
sudo pacman -S --needed --noconfirm awesome-terminal-fonts powerline-fonts

# Install OhMyZsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install syntax-highlighting and autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Install p10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Now edit ~/.zshrc and change ZSH_THEME="powerlevel10k/powerlevel10k"

# Finally run the config wizard with p10k configure