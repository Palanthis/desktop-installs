#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.reflector

reflector --protocol https --country 'United States' --latest 25 --age 12 --sort rate --save /etc/pacman.d/mirrorlist
