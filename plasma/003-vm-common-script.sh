#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm edk2-shell wget git

# Sound
sudo pacman -S --needed --noconfirm pipewire wireplumber gst-plugin-pipewire pipewire-alsa
sudo pacman -S --needed --noconfirm pipewire-jack pipewire-pulse

# Full QT5 Suite
sudo pacman -S --needed --noconfirm qt5

# Plasma from Standard Repos
sudo pacman -S --needed --noconfirm plasma-meta

# Plasma everything (has dupes, don't care)
sudo pacman -S --needed --noconfirm aspell-en bluedevil breeze
sudo pacman -S --needed --noconfirm breeze-gtk cdrdao qtcurve-kde
sudo pacman -S --needed --noconfirm discover dolphin-plugins k3b
sudo pacman -S --needed --noconfirm drkonqi gwenview kup
sudo pacman -S --needed --noconfirm dvd+rw-tools kde-gtk-config
sudo pacman -S --needed --noconfirm kde-applications-meta kdeconnect kfind
sudo pacman -S --needed --noconfirm kdegraphics-thumbnailers kopete
sudo pacman -S --needed --noconfirm kdelibs4support kdeplasma-addons
sudo pacman -S --needed --noconfirm kdialog kgamma5 ktorrent kgpg
sudo pacman -S --needed --noconfirm khelpcenter kinfocenter konsole
sudo pacman -S --needed --noconfirm kmenuedit kscreen
sudo pacman -S --needed --noconfirm ksshaskpass ksysguard milou
sudo pacman -S --needed --noconfirm kwrited krita kate
sudo pacman -S --needed --noconfirm kwalletmanager okular oxygen
sudo pacman -S --needed --noconfirm nm-connection-editor kalarm
sudo pacman -S --needed --noconfirm kwallet-pam sweeper sddm-kcm
sudo pacman -S --needed --noconfirm packagekit-qt5 plasma-pa sddm
sudo pacman -S --needed --noconfirm partitionmanager plasma-desktop
sudo pacman -S --needed --noconfirm plasma-nm plasma-sdk spectacle
sudo pacman -S --needed --noconfirm plasma-vault powerdevil k3b
sudo pacman -S --needed --noconfirm plasma-workspace qtcurve-gtk2
sudo pacman -S --needed --noconfirm plasma-workspace-wallpapers

# Wayland support
sudo pacman -S --needed --noconfirm plasma-wayland-protocols plasma-wayland-session qt5-wayland 

# Remove metas (They can cause dep problems later)
sudo pacman -R --noconfirm kde-applications-meta
sudo pacman -R --noconfirm kde-multimedia-meta
sudo pacman -R --noconfirm plasma-meta
sudo pacman -R --noconfirm kmix

# Kvantum
sudo pacman -S --needed --noconfirm kvantum

# Enable Display Manager
sudo systemctl enable sddm.service

# Fonts from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm keepassxc kid3-qt samba conky ffmpegthumbs
sudo pacman -S --needed --noconfirm file-roller calibre gnome-disk-utility
sudo pacman -S --needed --noconfirm uget gparted vlc ttf-liberation cryfs
sudo pacman -S --needed --noconfirm neofetch phonon-qt5-vlc soundconverter
sudo pacman -S --needed --noconfirm cairo-dock cairo-dock-plug-ins lolcat
sudo pacman -S --needed --noconfirm vorbis-tools libogg lib32-libogg opus-tools
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector os-prober
sudo pacman -S --needed --noconfirm firefox ntfs-3g qt5ct grub-customizer
sudo pacman -S --needed --noconfirm p7zip tlp smb4k adapta-gtk-theme powerline-fonts
sudo pacman -S --needed --noconfirm geany geany-plugins bleachbit awesome-terminal-fonts 

# YTDL Stuff
sudo pacman -S --needed --noconfirm yt-dlp python-pip python-websockets
/usr/bin/yes | pip install mutagen

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release f2fs-tools
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan grsync
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter putty whois openssh dialog

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf networkmanager-openvpn

# Optional - install xdg-user-dirs and update my home directory.
#This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf ../../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../../tarballs/buuf-icons-for-plasma.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../../tarballs/buuf3.42.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../../tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf ../../tarballs/arch-logo.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../../tarballs/smb.conf.tar.gz -C /etc/samba/ --overwrite
sudo tar xzf ../../tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf ../../tarballs/share.tar.gz -C /usr/share/ --overwrite
sudo tar xzf ../../tarballs/zsh.tar.gz -C ~/ --overwrite

# Copy over GRUB theme
sudo tar xzf ../../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Copy over SDDM theme
sudo tar xzf ../../tarballs/archlinux-sddm-theme.tar.gz -C /usr/share/sddm/themes/ --overwrite

# Copy SDDM config
sudo tar xzf ../../tarballs/sddm-conf.tar.gz -C /etc/ --overwrite

# Add screenfetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Plasma permissions error
sudo chown -R ghost:ghost /home/ghost
sudo chmod 770 /home/ghost

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."
sudo reboot
