#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S --noconfirm --needed xorg-server xorg-apps xorg-xinit
sudo pacman -S --noconfirm --needed xorg-twm xorg-xclock xterm
sudo pacman -S --noconfirm --needed linux-headers

# Copy Nvidia conf file (archrig only)
sudo cp ../AdditionalFiles/20-nvidia.conf /etc/X11/xorg.conf.d/

# Uncomment the below line for NVidia Drivers
sudo pacman -S --noconfirm --needed nvidia nvidia-settings
sudo pacman -S --noconfirm --needed nvidia-utils lib32-nvidia-utils
sudo pacman -S --noconfirm --needed lib32-opencl-nvidia opencl-nvidia
sudo pacman -S --noconfirm --needed lib32-libvdpau libxnvctrl libvdpau

# Optional WiFi Drivers
sudo pacman -S broadcom-wl-dkms --noconfirm --needed

# Install Yay
sudo pacman -U ../../packages/yay-12.3.5-1-x86_64.pkg.tar.zst --noconfirm

# Call common script
sh ../003-common-script.sh
