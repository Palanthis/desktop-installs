#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

pacman -S --needed --noconfirm reflector

mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.reflector

reflector --protocol http --protocol https --country 'United States' --latest 25 --age 4 --sort rate --save /etc/pacman.d/mirrorlist
