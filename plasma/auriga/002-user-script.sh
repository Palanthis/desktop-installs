#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed

# Uncomment the below line for AMD Video
sudo pacman -S --noconfirm --needed mesa vdpauinfo
sudo pacman -S --noconfirm --needed xf86-video-amdgpu xf86-video-ati vulkan-radeon lib32-vulkan-radeon
sudo pacman -S --noconfirm --needed libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau

# Bluetooth
sudo pacman -S --noconfirm --needed bluez bluez-utils
sudo systemctl enable bluetooth.service

# Install Yay
sudo pacman -U ../../packages/yay-11.2.0-1-x86_64.pkg.tar.xz --noconfirm

# Call common script
sh ../003-common-script.sh
