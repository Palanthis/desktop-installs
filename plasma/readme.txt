What's up with these folders?
	They are named after my own personal computers, which all run Arch Linux. They all end up running the same scripts for 99% of the things that they install and configure. Here are the differences.

ArchBookPro - This is a MacBook Pro. The script installs the Intel driver, the Intel Hybrid codec (for hardware encode / decode) and enables an anti-tearing config for xorg. 

ArchMini - This is a Dell mini PC. It is the same as the MacBook script.

ArchRig - This is a custom gaming rig. It (currently) runs a GeForce GTX 1080 Ti. This installs the full NVidia driver, as well as the stuff from multilib to ensure Steam works correctly. It also drops an xorg config for my setup, which is dual-monitor and forces full_composition_pipeline for both monitors. You can comment that out if you want. It won't hurt anything. It was done way back to fix tearing under certain circumstances. It also has a PCIe WiFi card. This installs the Broadcom driver for it.

Auriga - This is an AsRock DeskMini with a Ryzen Pro. It installs the ATI and AMD GPU drivers and their 32-bit components. Again, so Steam works.

KVM - This is for KVM / QEMU deployments. It installs the guest drivers. It also calls the slimmed down vm-script, which is lighter on apps and does not include virtualization or printing support.

Nostromo - Identical to ArchMini. I don't know why I still have both there, really. Just lazy, I suppose. Or maybe I was going to do more system-specific tweaks. I don't remember.

VBox - This is for VirtualBox deployments. It installs the vbox guest drivers and then calls the same (lighter) VM script that KVM does. So no virtualization or printing support.