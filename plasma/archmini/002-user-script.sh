#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# Uncomment the below line for Intel Video
sudo pacman -S --noconfirm --needed mesa xf86-video-intel vdpauinfo
sudo pacman -S --noconfirm --needed libva-intel-driver intel-media-driver
sudo pacman -S --noconfirm --needed libvdpau-va-gl libva-utils
sed -i 's/#footrizen/trizen/' ../003-common-script.sh

# Bluetooth
sudo pacman -S --noconfirm --needed bluez bluez-utils
sudo systemctl enable bluetooth.service

# Copy over Intel xorg file (fix screen tearing)
sudo cp ../AdditionalFiles/20-intel.conf /usr/share/X11/xorg.conf.d/

# Install Yay
sudo pacman -U ../../packages/yay-11.2.0-1-x86_64.pkg.tar.xz --noconfirm

# Call common script
sh ../003-common-script.sh
