#!/bin/bash
set -e

# Removes old packages and databases
sudo pacman -Scc --noconfirm

# Sync all repos and generate updated DBs
sudo pacman -Syy

# Update everything
sudo pacman -Su --noconfirm
