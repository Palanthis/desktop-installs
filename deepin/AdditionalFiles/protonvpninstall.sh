#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

git clone "https://github.com/protonvpn/protonvpn-cli"
cd protonvpn-cli
sudo ./protonvpn-cli.sh --install
