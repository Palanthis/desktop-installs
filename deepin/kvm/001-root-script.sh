#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "###################################"
echo "#### This must be run as root  ####"
echo "####   using su, NOT sudo     #####"
echo "###################################"

# Pacman Init
pacman-key --init
pacman-key --populate archlinux

# Fix gpg import error
tar xzf ../tarballs/gpg.tar.gz -C /etc/pacman.d/gnupg/ --overwrite

# Ensure pacman cache is up-to-date
pacman -Syy

# Install ccache
pacman -S ccache --needed --noconfirm

# Copy over makepkg.conf
cp -f makepkg.conf /etc/makepkg.conf

# Copy and enable reflector service
tar xzf ../tarballs/reflector-service.tar.gz -C /etc/systemd/system/ --overwrite
systemctl enable reflector.service

# Enable fstrim.timer
systemctl enable fstrim.timer

# Enable network time
cp -f timesyncd.conf /etc/systemd/

systemctl enable systemd-networkd.service

systemctl start systemd-networkd.service

systemctl enable systemd-timesyncd.service

systemctl start systemd-timesyncd.service

systemctl disable systemd-networkd-wait-online.service

timedatectl set-ntp true

hwclock --systohc --utc

echo " "
echo "Let's make sure everything looks right!"

timedatectl status
