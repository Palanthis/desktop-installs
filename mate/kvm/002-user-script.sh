#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm mesa --noconfirm --needed


# Install user agent
sudo pacman -S --noconfirm --needed qemu-guest-agent

# Install Yay
sudo pacman -U ../../packages/yay-11.2.0-1-x86_64.pkg.tar.xz --noconfirm

# Call common script
sh ../003-vm-common-script.sh
