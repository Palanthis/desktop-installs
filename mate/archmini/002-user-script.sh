#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Install trizen
sudo pacman -S --needed --noconfirm trizen

# Xorg Core
sudo pacman -S xorg-server xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# Uncomment the below line for Intel Video
sudo pacman -S --noconfirm --needed mesa xf86-video-intel
sudo pacman -S --noconfirm --needed libva-intel-driver intel-media-driver

# Bluetooth
sudo pacman -S --noconfirm --needed bluez bluez-utils
sudo systemctl enable bluetooth.service

# Install VirtualBox and optional components
sudo pacman -S --needed --noconfirm virtualbox virtualbox-host-dkms
sudo pacman -S --needed --noconfirm virtualbox-guest-iso

# Copy over Intel xorg file (fix screen tearing)
sudo cp ../AdditionalFiles/20-intel.conf /usr/share/X11/xorg.conf.d/

# Call common script
sh ../003-common-script.sh
