#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm archiso wget git

# Sound
sudo pacman -S --needed --noconfirm pipewire wireplumber gst-plugin-pipewire pipewire-alsa
sudo pacman -S --needed --noconfirm pipewire-jack pipewire-pulse

# Install Mate
sudo pacman -S --needed --noconfirm mate mate-extra lightdm lightdm-gtk-greeter
sudo pacman -S --needed --noconfirm lightdm-gtk-greeter-settings ntfs-3g gvfs gvfs-smb gvfs-nfs

# Enable Display Manager
sudo systemctl enable lightdm.service

# Fonts from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm keepassxc kid3-qt samba conky veracrypt remmina
sudo pacman -S --needed --noconfirm file-roller evince calibre gnome-disk-utility
sudo pacman -S --needed --noconfirm uget gparted vlc ttf-liberation hunspell-en_us
sudo pacman -S --needed --noconfirm neofetch phonon-qt5-vlc soundconverter awesome-terminal-fonts
sudo pacman -S --needed --noconfirm cairo-dock cairo-dock-plug-ins pitivi powerline-fonts
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg freerdp
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector handbrake soundconverter
sudo pacman -S --needed --noconfirm clementine firefox audacity ntfs-3g qt5ct vinagre
sudo pacman -S --needed --noconfirm steam steam-native-runtime p7zip tlp bleachbit
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot libbluray smb4k
sudo pacman -S --needed --noconfirm variety meld gimp inkscape openshot libdvdcss
sudo pacman -S --needed --noconfirm shotwell simplescreenrecorder lolcat opus-tools
sudo pacman -S --needed --noconfirm rhythmbox adapta-gtk-theme geany geany-plugins

# YTDL Stuff
sudo pacman -S --needed --noconfirm yt-dlp python-pip python-websockets
/usr/bin/yes | pip install mutagen

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release f2fs-tools
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan grsync
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter putty whois openssh dialog

# Cockpit
sudo pacman -S --needed --noconfirm cockpit
sudo systemctl enable --now cockpit.socket

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf networkmanager-openvpn

# Kvantum
sudo pacman -S --needed --noconfirm kvantum

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode nanhtop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter whois gmrun dialog

# Install Libre Office or Free Office
sudo pacman -S --needed --noconfirm libreoffice-fresh
# trizen -S freeoffice --noconfirm

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf
sudo pacman -S --needed --noconfirm networkmanager-openvpn

# Printing Support
sudo pacman -S --needed --noconfirm cups cups-pdf libcups hplip
sudo pacman -S --needed --noconfirm foomatic-db-engine
sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds
sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds
sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds
sudo pacman -S --needed --noconfirm ghostscript gsfonts gutenprint
sudo pacman -S --needed --noconfirm gtk3-print-backends
sudo pacman -S --needed --noconfirm system-config-printer
# Fix This - sudo systemctl enable org.cups.cupsd.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/arch ] || sudo mkdir /usr/share/icons/arch
sudo tar xzf ../../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../../tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../../tarballs/archlogo.tar.gz -C /usr/share/icons/arch/ --overwrite

# Write out new configuration files
#tar xzf ../../tarballs/config.tar.gz -C ~/ --overwrite
#tar xzf ../../tarballs/local.tar.gz -C ~/ --overwrite

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf ../../tarballs/conky.tar.gz -C ~/.conky/

# Copy over GRUB theme
sudo tar xzf ../../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Add neofetch and lolcat
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix)
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

# Virtualbox Compton Fix
#foosed -i 's/vsync/#vsync/' ~/.config/compton.conf
#foosed -i 's/glx/xrender/' ~/.config/compton.conf

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
