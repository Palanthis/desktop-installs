#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Plasma from AUR
trizen -S --needed --noconfirm pamac-tray-appindicator

# Virtualbox Extensions
trizen -S --noconfirm --needed virtualbox-ext-oracle 

# Apps from AUR
trizen -S --noconfirm --needed chromium-widevine
trizen -S --noconfirm --needed gitkraken
trizen -S --noconfirm --needed caffeine-ng
trizen -S --noconfirm --needed megasync-git
trizen -S --noconfirm --needed rar
trizen -S --noconfirm --needed youtube-dl-gui-git
