#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v2
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm archiso wget git

# Install trizen
sudo pacman -S --needed --noconfirm trizen

# Install octopi and package-query
trizen -S --needed --noconfirm package-query
#trizen -S --needed --noconfirm octopi
#trizen -S --needed --noconfirm alpm_octopi_utils

# Install pamac
sudo pacman -S --needed --noconfirm pamac-aur

# Install OpenBox and Core Applications
sudo pacman -S --needed --noconfirm sddm pcmanfm-gtk3 rofi ntfs-3g gtk2
sudo pacman -S --needed --noconfirm gvfs obconf xarchiver gvfs-smb
sudo pacman -S --needed --noconfirm gvfs-smb gvfs-nfs volumeicon gtk3
sudo pacman -S --needed --noconfirm openbox tint2 nitrogen compton 
sudo pacman -S --needed --noconfirm lxappearance-obconf oblogout
sudo pacman -S --needed --noconfirm perl-data-dump gtk2-perl xscreensaver
sudo pacman -S --needed --noconfirm perl-file-desktopentry polkit-gnome

# Install OpenBox Extras
trizen -S --noconfirm obmenu
trizen -S --noconfirm obmenu-generator
trizen -S --noconfirm obbrowser
trizen -S --noconfirm perl-linux-desktopfiles
trizen -S --noconfirm comptray
trizen -S --noconfirm gksu

# Enable Display Manager
sudo systemctl enable sddm.service

# Sound
sudo pacman -S --needed --noconfirm pulseaudio pulseaudio-alsa pavucontrol
sudo pacman -S --needed --noconfirm alsa-utils alsa-plugins alsa-lib
sudo pacman -S --needed --noconfirm alsa-firmware gst-plugins-ugly
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad
sudo pacman -S --needed --noconfirm gst-plugins-base gstreamer kid3-qt

# Software from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
# sudo pacman -S --needed --noconfirmadapta-gtk-theme
sudo pacman -S --needed --noconfirm qt5-styleplugins qt5ct adobe-source-code-pro-fonts

# Adapta Theme
#sudo tar xzf ../tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite
sudo pacman -S --needed --noconfirm adapta-gtk-theme

# Apps from standard repos
sudo pacman -S --needed --noconfirm geany geany-plugins keepassxc
sudo pacman -S --needed --noconfirm conky conky-manager file-roller evince
sudo pacman -S --needed --noconfirm uget deluge gparted vlc redshift
sudo pacman -S --needed --noconfirm tilix neofetch gnome-disk-utility
sudo pacman -S --needed --noconfirm lolcat firefox plank ttf-liberation
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector shotwell
sudo pacman -S --needed --noconfirm rhythmbox simplescreenrecorder audacity
sudo pacman -S --needed --noconfirm steam steam-native-runtime p7zip
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot
sudo pacman -S --needed --noconfirm variety meld gimp inkscape openshot

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode hardinfo htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter whois gmrun dialog

# Install Libre Office
sudo pacman -S --needed --noconfirm libreoffice-fresh

# Install VirtualBox and optional components
sudo pacman -S --needed --noconfirm virtualbox virtualbox-host-dkms
sudo pacman -S --needed --noconfirm virtualbox-guest-iso

# Apps from AUR (moved to 004 script)
#trizen -S --noconfirm chromium-widevine
#trizen -S --noconfirm gitkraken
#trizen -S --noconfirm caffeine-ng
#trizen -S --noconfirm megasync-git
#trizen -S --noconfirm etcher
#trizen -S --noconfirm rar

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf
sudo pacman -S --needed --noconfirm networkmanager-openvpn

# Printing Support
sudo pacman -S --needed --noconfirm cups cups-pdf libcups hplip
sudo pacman -S --needed --noconfirm foomatic-db-engine
sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds
sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds
sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds
sudo pacman -S --needed --noconfirm ghostscript gsfonts gutenprint
sudo pacman -S --needed --noconfirm gtk3-print-backends
sudo pacman -S --needed --noconfirm system-config-printer
sudo systemctl enable org.cups.cupsd.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/arch ] || sudo mkdir /usr/share/icons/arch
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/archlogo.tar.gz -C /usr/share/icons/arch/ --overwrite
sudo tar xzf ../tarballs/oblogout.tar.gz -C /usr/share/themes/ --overwrite

# Write out new configuration files
tar xzf ../tarballs/config.tar.gz -C ~/ --overwrite
tar xzf ../tarballs/gtkrc.tar.gz -C ~/ --overwrite

# Change oblogout theme
sudo tar xzf ../tarballs/oblogout-conf.tar.gz -C /etc --overwrite

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf ../tarballs/conky.tar.gz -C ~/.conky/

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Copy over SDDM theme
sudo tar xzf ../tarballs/archlinux-sddm-theme.tar.gz -C /usr/share/sddm/themes/ --overwrite

# Copy SDDM config
sudo tar xzf ../tarballs/sddm-conf.tar.gz -C /etc/ --overwrite

# Remove special repo from pacman.conf (only used to install pamac and trizen)
sudo mv /etc/pacman.conf /etc/pacman.old
sudo tar xzf ../tarballs/pacman.conf.tar.gz -C /etc/ --overwrite

# Add neofetch and lolcat
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix)
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

# Virtualbox Compton Fix
#foosed -i 's/vsync/#vsync/' ~/.config/compton.conf
#foosed -i 's/glx/xrender/' ~/.config/compton.conf

# Additional hardware decoding for Intel chips.
#footrizen -S --noconfirm intel-hybrid-codec-driver

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
