#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm wget git


# Install Gnome and Core Applications
sudo pacman -S --needed --noconfirm gdm gnome gnome-extra rofi ntfs-3g
sudo pacman -S --needed --noconfirm gvfs gvfs-nfs gvfs-smb

# Enable Display Manager
sudo systemctl enable gdm.service

# Sound
sudo pacman -S --needed --noconfirm alsa-utils alsa-plugins alsa-lib
sudo pacman -S --needed --noconfirm alsa-firmware gst-plugins-ugly
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad
sudo pacman -S --needed --noconfirm gst-plugins-base gstreamer kid3-qt

# Software from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm qt5ct adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm geany geany-plugins keepassxc cryfs adapta-gtk-theme
sudo pacman -S --needed --noconfirm conky conky-manager file-roller evince smb4k freerdp vinagre
sudo pacman -S --needed --noconfirm uget deluge gparted vlc redshift ntfs-3g calibre inkscape
sudo pacman -S --needed --noconfirm tilix neofetch gnome-disk-utility tlp openshot gimp
sudo pacman -S --needed --noconfirm lolcat firefox plank ttf-liberation meld variety flameshot
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg os-prober
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector shotwell grub-customizer
sudo pacman -S --needed --noconfirm rhythmbox simplescreenrecorder audacity p7zip

# Kvantum
sudo pacman -S --needed --noconfirm kvantum-qt5

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode hardinfo htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter whois gmrun dialog

# Install Libre Office
sudo pacman -S --needed --noconfirm libreoffice-fresh

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf
sudo pacman -S --needed --noconfirm networkmanager-openvpn

# Printing Support
sudo pacman -S --needed --noconfirm cups cups-pdf libcups hplip
sudo pacman -S --needed --noconfirm foomatic-db-engine
sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds
sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds
sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds
sudo pacman -S --needed --noconfirm ghostscript gsfonts gutenprint
sudo pacman -S --needed --noconfirm gtk3-print-backends
sudo pacman -S --needed --noconfirm system-config-printer
sudo systemctl enable cups.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/arch ] || sudo mkdir /usr/share/icons/arch
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/archlogo.tar.gz -C /usr/share/icons/arch/ --overwrite

# Write out new configuration files
#tar xzf ../tarballs/config.tar.gz -C ~/ --overwrite
#tar xzf ../tarballs/local.tar.gz -C ~/ --overwrite

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
#tar xzf ../tarballs/conky.tar.gz -C ~/.conky/

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite


# Add neofetch and lolcat
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix)
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

# Virtualbox Compton Fix
#foosed -i 's/vsync/#vsync/' ~/.config/compton.conf
#foosed -i 's/glx/xrender/' ~/.config/compton.conf

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
