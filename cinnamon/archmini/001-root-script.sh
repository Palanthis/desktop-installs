#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "###################################"
echo "#### This must be run as root  ####"
echo "####   using su, NOT sudo     #####"
echo "###################################"

# Import Andontie Keys
pacman-key --recv-key EA50C866329648EE
pacman-key --lsign-key EA50C866329648EE

# Add andontie-aur repo
echo '[andontie-aur]' >> /etc/pacman.conf
echo 'Server = https://aur.andontie.net/$arch' >> /etc/pacman.conf

# Ensure pacman cache is up-to-date
pacman -Syy

# Install ccache
pacman -S ccache --needed --noconfirm

# Copy over makepkg.conf
cp -f makepkg.conf /etc/makepkg.conf

# Copy and enable reflector service
tar xzf ../tarballs/reflector-service.tar.gz -C /etc/systemd/system/ --overwrite
systemctl enable reflector.service

# Fix qt5ct setting
echo QT_QPA_PLATFORMTHEME=qt5ct >> /etc/environment

# Enable network time
systemctl enable systemd-networkd.service

systemctl start systemd-networkd.service

systemctl enable systemd-timesyncd.service

systemctl start systemd-timesyncd.service

timedatectl set-ntp true

hwclock --systohc --utc

echo " "
echo "Let's make sure everything looks right!"

timedatectl status
