#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

numberofcores=$(grep -c ^processor /proc/cpuinfo)


case $numberofcores in

    12)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 12 -z -)/g' /etc/makepkg.conf
        sudo sed -i 's/MAKEFLAGS="-jx -lx"/MAKEFLAGS="-j13 -l12"/g' /etc/makepkg.conf
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j13 -l12"' >> ~/.bashrc
        ;;
    8)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 8 -z -)/g' /etc/makepkg.conf
        sudo sed -i 's/MAKEFLAGS="-jx -lx"/MAKEFLAGS="-j9 -l8"/g' /etc/makepkg.conf
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j9 -l8"' >> ~/.bashrc
        ;;
    6)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 6 -z -)/g' /etc/makepkg.conf
        sudo sed -i 's/MAKEFLAGS="-jx -lx"/MAKEFLAGS="-j7 -l6"/g' /etc/makepkg.conf
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j7 -l6"' >> ~/.bashrc
        ;;
	4)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 4 -z -)/g' /etc/makepkg.conf
        sudo sed -i 's/MAKEFLAGS="-jx -lx"/MAKEFLAGS="-j5 -l4"/g' /etc/makepkg.conf
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j5 -l4"' >> ~/.bashrc
        ;;
    2)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        sudo sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 2 -z -)/g' /etc/makepkg.conf
        sudo sed -i 's/MAKEFLAGS="-jx -lx"/MAKEFLAGS="-j3 -l2"/g' /etc/makepkg.conf
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j3 -l2"' >> ~/.bashrc
        ;;
    *)
        echo "We do not know how many cores you have."
        echo "Do it manually."
        ;;

esac

echo "################################################################"
echo "###  All cores will be used during building and compression ####"
echo "################################################################"
