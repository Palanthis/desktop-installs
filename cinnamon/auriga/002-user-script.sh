#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed

# Uncomment the below line for AMD Video
sudo pacman -S --noconfirm --needed mesa vdpauinfo libva-mesa-driver mesa-vdpau
sudo pacman -S --noconfirm --needed xf86-video-amdgpu vulkan-radeon lib32-vulkan-radeon
sudo pacman -S --noconfirm --needed lib32-libva-mesa-driver lib32-mesa-vdpau

# Bluetooth
sudo pacman -S --noconfirm --needed bluez bluez-utils
sudo systemctl enable bluetooth.service

sudo pacman -U ../../packages/yay-10.2.0-1-x86_64.pkg.tar.xz --noconfirm

# Call common script
sh ../003-common-script.sh
