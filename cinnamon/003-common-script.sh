#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules wget git

# Install Cinnamon and Core Applications
sudo pacman -S --needed --noconfirm cinnamon rofi ntfs-3g gvfs gvfs-nfs gvfs-smb
sudo pacman -S --needed --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings

# Enable Display Manager
sudo systemctl enable lightdm.service

# Sound
sudo pacman -S --needed --noconfirm alsa-firmware gst-plugins-ugly gst-plugins-base
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad
sudo pacman -S --needed --noconfirm alsa-utils alsa-plugins alsa-lib gstreamer

# Software from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm geany geany-plugins keepassxc cryfs handbrake kid3-qt
sudo pacman -S --needed --noconfirm conky file-roller evince tlp ntfs-3g calibre samba openshot
sudo pacman -S --needed --noconfirm uget qbittorrent gparted vlc redshift qt5ct smb4k variety
sudo pacman -S --needed --noconfirm tilix neofetch gnome-disk-utility qt5-tools gnome-keyring
sudo pacman -S --needed --noconfirm lolcat firefox plank ttf-liberation phonon-qt5-vlc
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg soundconverter
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector shotwell vinagre rhythmbox
sudo pacman -S --needed --noconfirm clementine simplescreenrecorder audacity freerdp inkscape
sudo pacman -S --needed --noconfirm steam steam-native-runtime p7zip grsync gimp meld
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot adapta-gtk-theme

# Kvantum
sudo pacman -S --needed --noconfirm kvantum-qt5

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer dialog tumbler
sudo pacman -S --needed --noconfirm dmidecode hardinfo htop mlocate gmrun wmctrl
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release whois vnstat scrot
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan unclutter sysstat

# Install Libre Office or Free Office
sudo pacman -S --needed --noconfirm libreoffice-fresh

# Install all needed packages for full QEMU/KVM support
sudo pacman -S --needed --noconfirm ebtables iptables dnsmasq vde2 virt-manager
sudo pacman -S --needed --noconfirm qemu libvirt edk2-ovmf qemu-arch-extra
sudo pacman -S --needed --noconfirm qemu-block-iscsi qemu-guest-agent

# Enable libnvirt daemon
sudo systemctl enable libvirtd.service

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf networkmanager-openvpn

# Printing Support
sudo pacman -S --needed --noconfirm cups cups-pdf libcups hplip foomatic-db-engine
sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds gtk3-print-backends
sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds ghostscript gsfonts
sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds gutenprint system-config-printer
sudo systemctl enable cups.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/arch ] || sudo mkdir /usr/share/icons/arch
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/archlogo.tar.gz -C /usr/share/icons/arch/ --overwrite

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf ../tarballs/conky.tar.gz -C ~/.conky/

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Add neofetch and lolcat
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix)
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
