#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm edk2-shell wget git

# Install Xfce core applications
sudo pacman -S --needed --noconfirm xfce4 xfce4-goodies ntfs-3g gvfs gvfs-smb gvfs-nfs numlockx
sudo pacman -S --needed --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings 

# Enable Display Manager
sudo systemctl enable lightdm.service

# Sound
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad gst-plugins-base
sudo pacman -S --needed --noconfirm gst-plugins-ugly gstreamer

# Software from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm geany geany-plugins keepassxc cryfs handbrake
sudo pacman -S --needed --noconfirm conky file-roller evince tlp ntfs-3g calibre
sudo pacman -S --needed --noconfirm uget qbittorrent gparted vlc redshift
sudo pacman -S --needed --noconfirm tilix neofetch gnome-disk-utility qt5-tools
sudo pacman -S --needed --noconfirm lolcat firefox plank ttf-liberation
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector shotwell
sudo pacman -S --needed --noconfirm clementine simplescreenrecorder audacity
sudo pacman -S --needed --noconfirm steam steam-native-runtime p7zip grsync
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot
sudo pacman -S --needed --noconfirm variety meld gimp inkscape openshot
sudo pacman -S --needed --noconfirm adapta-gtk-theme freerdp vinagre rhythmbox
sudo pacman -S --needed --noconfirm gnome-keyring smb4k soundconverter
sudo pacman -S --needed --noconfirm kid3-qt samba qt5ct phonon-qt5-vlc

# Kvantum
sudo pacman -S --needed --noconfirm kvantum-qt5

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer dialog
sudo pacman -S --needed --noconfirm dmidecode htop mlocate gmrun
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release whois
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan unclutter
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl

# Install Libre Office or Free Office (Disabled for VMs)
#sudo pacman -S --needed --noconfirm libreoffice-fresh
# trizen -S freeoffice --noconfirm

# Install all needed packages for full QEMU/KVM support (Disabled for VMs)
#sudo pacman -S --needed --noconfirm ebtables iptables dnsmasq vde2 virt-manager
#sudo pacman -S --needed --noconfirm qemu libvirt edk2-ovmf qemu-arch-extra
#sudo pacman -S --needed --noconfirm qemu-block-iscsi qemu-guest-agent

# Enable libnvirt daemon
#sudo systemctl enable libvirtd.service

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf
sudo pacman -S --needed --noconfirm networkmanager-openvpn

# Printing Support (Disabled for VMs)
#sudo pacman -S --needed --noconfirm cups cups-pdf libcups hplip
#sudo pacman -S --needed --noconfirm foomatic-db-engine
#sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds
#sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds
#sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds
#sudo pacman -S --needed --noconfirm ghostscript gsfonts gutenprint
#sudo pacman -S --needed --noconfirm gtk3-print-backends
#sudo pacman -S --needed --noconfirm system-config-printer
#sudo systemctl enable cups.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Optional - install xdg-user-dirs and update my home directory.
# This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/arch ] || sudo mkdir /usr/share/icons/arch
[ -d ~/Templates ] || mkdir ~/Templates
sudo tar xzf ../../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../../tarballs/buuf3.42.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../../tarballs/archlogo.tar.gz -C /usr/share/icons/arch/ --overwrite
tar xzf ../../tarballs/templates.tar.gz -C ~/Templates/ --overwrite

# Write out new configuration files
#tar xzf ../../tarballs/config.tar.gz -C ~/ --overwrite
#tar xzf ../../tarballs/local.tar.gz -C ~/ --overwrite

# Copy over Menulibre items
[ -d ~/.local/share ] || mkdir -p ~/.local/share
[ -d ~/.config/menus ] || mkdir -p ~/.config/menus
tar xzf ../../tarballs/applications.tar.gz -C ~/.local/share/ --overwrite
tar xzf ../../tarballs/xfce-applications-menu.tar.gz -C ~/.config/menus/ --overwrite

# Copy Conky
[ -d ~/.conky ] || mkdir ~/.conky
#tar xzf ../../tarballs/conky.tar.gz -C ~/.conky/

# Additional hardware decoding for Intel chips.
#footrizen -S --noconfirm intel-hybrid-codec-driver-gcc10

# Copy over GRUB theme
sudo tar xzf ../../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Configure LightDM
sudo [ -d /etc/lightdm ] || sudo mkdir /etc/lightdm
sudo cp ../../fun-stuff/lightdm.conf /etc/lightdm/lightdm.conf

# Add neofetch and lolcat
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Tilix)
echo 'if [ $TILIX_ID ] || [ $VTE_VERSION ]; then source /etc/profile.d/vte.sh
fi' >> ~/.bashrc

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
