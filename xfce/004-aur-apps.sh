#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install pamac
yay -S --needed --noconfirm package-query
yay -S --needed --noconfirm pamac-aur

# Apps from AUR
yay -S --noconfirm --needed gitkraken
yay -S --noconfirm --needed caffeine-ng
yay -S --noconfirm --needed rar
yay -S --noconfirm --needed youtube-dl-gui-git
yay -S --noconfirm --needed tlpui-git
