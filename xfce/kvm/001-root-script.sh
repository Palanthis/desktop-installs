#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "###################################"
echo "#### This must be run as root  ####"
echo "####   using su, NOT sudo     #####"
echo "###################################"

# Pacman Init
pacman-key --init
pacman-key --populate archlinux

# Install reflector (mirrorlist generator)
pacman -S --needed --noconfirm reflector

# Backup current mirrorlist
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.reflector

# Generate new mirrorlist
reflector --protocol http --protocol https --country 'United States' --latest 25 --age 4 --sort rate --save /etc/pacman.d/mirrorlist

# Fix gpg import error
tar xzf ../../tarballs/gpg.tar.gz -C /etc/pacman.d/gnupg/ --overwrite

# Ensure pacman cache is up-to-date
pacman -Syy

# Install ccache
pacman -S ccache --needed --noconfirm

# Copy over makepkg.conf
cp -f makepkg.conf /etc/makepkg.conf

# Copy and enable reflector service
tar xzf ../../tarballs/reflector-service.tar.gz -C /etc/systemd/system/ --overwrite
systemctl enable reflector.service

# Enable filesystem trim
systemctl enable fstrim.timer

# Enable network time
cp -f timesyncd.conf /etc/systemd/

systemctl enable --now systemd-networkd.service

systemctl enable --now systemd-timesyncd.service

# Disable wait-online. It's pointless and can cause issues with VPNs
systemctl disable systemd-networkd-wait-online.service

# Enable network time
timedatectl set-ntp true

# Set the cmos clock to UTC
hwclock --systohc --utc

echo " "
echo "Let's make sure everything looks right!"

# Check that the time and date are correct
timedatectl status
