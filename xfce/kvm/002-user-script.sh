#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S --noconfirm --needed xorg-server xorg-apps xorg-xinit
sudo pacman -S --noconfirm --needed xorg-twm xorg-xclock xterm

# Call common script
sh ../004-vm-common-script.sh
