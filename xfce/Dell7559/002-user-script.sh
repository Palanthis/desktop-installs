#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Use all cores for make and compress
./use-all-cores-makepkg.sh

# Xorg Core
sudo pacman -S --noconfirm --needed xorg-server xorg-apps xorg-xinit
sudo pacman -S --noconfirm --needed xorg-twm xorg-xclock xterm
sudo pacman -S --noconfirm --needed linux-headers

# Uncomment the below line for NVidia Drivers
#sudo pacman -S --noconfirm --needed nvidia-dkms nvidia-settings
#sudo pacman -S --noconfirm --needed nvidia-utils lib32-nvidia-utils
#sudo pacman -S --noconfirm --needed lib32-opencl-nvidia opencl-nvidia
#sudo pacman -S --noconfirm --needed lib32-libvdpau libxnvctrl libvdpau

# Bumblebee
#sudo pacman -S bumblebee primus lib32-primus bbswitch --noconfirm --needed

# Uncomment the below line for Intel Video
sudo pacman -S --noconfirm --needed mesa xf86-video-intel vdpauinfo
sudo pacman -S --noconfirm --needed libva-intel-driver intel-media-driver
sudo pacman -S --noconfirm --needed libvdpau-va-gl libva-utils
sed -i 's/#footrizen/trizen/' ../003-common-script.sh

# Bluetooth
sudo pacman -S bluez bluez-utils
sudo systemctl enable bluetooth.service

# Copy over Intel xorg file (fix screen tearing)
#sudo cp ../AdditionalFiles/20-intel.conf /usr/share/X11/xorg.conf.d/

# Copy Nvidia conf file (archrig only)
#sudo cp ../AdditionalFiles/20-nvidia.conf /usr/share/X11/xorg.conf.d/
#sudo mv /usr/share/X11/xorg.conf.d/20-nvidia.conf /usr/share/X11/xorg.conf.d/30-nvidia.conf

# Call common script
sh ../003-common-script.sh
