#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm edk2-shell wget git

# Pantheon Desktop
sudo pacman -S --needed --noconfirm pantheon
#sudo pacman -S --needed --noconfirm cerbere gala lightdm-pantheon-greeter switchboard lightdm
#sudo pacman -S --needed --noconfirm minder pantheon-applications-menu pantheon-calculator
#sudo pacman -S --needed --noconfirm pantheon-calenda pantheon-camera pantheon-code
#sudo pacman -S --needed --noconfirm pantheon-default-settings pantheon-files pantheon-geoclue2-agent
#sudo pacman -S --needed --noconfirm pantheon-mail pantheon-music pantheon-notifications
#sudo pacman -S --needed --noconfirm pantheon-onboarding pantheon-photos pantheon-polkit-agent
#sudo pacman -S --needed --noconfirm pantheon-print pantheon-screenshot pantheon-session
#sudo pacman -S --needed --noconfirm pantheon-settings-daemon pantheon-shortcut-overlay
#sudo pacman -S --needed --noconfirm pantheon-sideload pantheon-tasks pantheon-terminal
#sudo pacman -S --needed --noconfirm pantheon-videos

# Enable Display Manager
sudo systemctl enable lightdm.service

# Sound
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad gst-plugins-base
sudo pacman -S --needed --noconfirm gst-plugins-ugly gstreamer

# Fonts from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm keepassxc kid3-qt samba conky vinagre
sudo pacman -S --needed --noconfirm file-roller evince calibre gnome-disk-utility
sudo pacman -S --needed --noconfirm uget gparted vlc ttf-liberation cryfs
sudo pacman -S --needed --noconfirm neofetch phonon-qt5-vlc soundconverter
sudo pacman -S --needed --noconfirm cairo-dock cairo-dock-plug-ins rhythmbox
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector handbrake
sudo pacman -S --needed --noconfirm clementine firefox audacity ntfs-3g qt5ct
sudo pacman -S --needed --noconfirm p7zip tlp smb4k freerdp adapta-gtk-theme
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot
sudo pacman -S --needed --noconfirm variety meld gimp inkscape openshot
sudo pacman -S --needed --noconfirm shotwell simplescreenrecorder lolcat

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode hardinfo htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release f2fs-tools
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan grsync
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter putty whois openssh dialog

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf networkmanager-openvpn

# QEMU Guest Agent
sudo pacman -S --needed --noconfirm qemu-guest-agent

# Optional - install xdg-user-dirs and update my home directory.
#This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/Buuf-Plasma-1.7.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf ../tarballs/arch-logo.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/smb.conf.tar.gz -C /etc/samba/ --overwrite
sudo tar xzf ../tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf ../tarballs/share.tar.gz -C /usr/share/ --overwrite

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Add screenfetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Plasma permissions error
sudo chown -R ghost:ghost /home/ghost
sudo chmod 770 /home/ghost

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."
sudo reboot
