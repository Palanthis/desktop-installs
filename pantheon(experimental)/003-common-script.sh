#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Network
sudo pacman -S --needed --noconfirm networkmanager
sudo pacman -S --needed --noconfirm network-manager-applet
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# Install build tools
sudo pacman -S --needed --noconfirm cmake ccache extra-cmake-modules
sudo pacman -S --needed --noconfirm edk2-shell wget git

# IPTables NFT (Keep Plasma from pulling in old version)
#sudo pacman -S --needed --noconfirm iptables-nft

# Full QT5 Suite
sudo pacman -S --needed --noconfirm qt5

# Plasma from Standard Repos
sudo pacman -S --needed --noconfirm plasma

# Plasma everything (has dupes, don't care)
sudo pacman -S --needed --noconfirm plasma-meta
sudo pacman -S --needed --noconfirm aspell-en bluedevil breeze qtcurve-gtk2 kopete plasma-workspace
sudo pacman -S --needed --noconfirm breeze-gtk cdrdao qtcurve-kde discover dolphin-plugins k3b
sudo pacman -S --needed --noconfirm drkonqi gwenview ffmpegthumbs dvd+rw-tools kde-gtk-config
sudo pacman -S --needed --noconfirm kde-applications-meta kdeconnect kfind kdegraphics-thumbnailers
sudo pacman -S --needed --noconfirm kdelibs4support kdeplasma-addons kdialog kgamma5 ktorrent kgpg
sudo pacman -S --needed --noconfirm khelpcenter kinfocenter konsole kipi-plugins kmenuedit kscreen
sudo pacman -S --needed --noconfirm ksshaskpass ksysguard kwin milou kwrited kdepim krita kate
sudo pacman -S --needed --noconfirm kwalletmanager okular oxygen nm-connection-editor sweeper
sudo pacman -S --needed --noconfirm kwallet-pam sddm-kcm packagekit-qt5 plasma-pa sddm powerdevil
sudo pacman -S --needed --noconfirm partitionmanager plasma-desktop plasma-nm plasma-sdk spectacle
sudo pacman -S --needed --noconfirm plasma-workspace-wallpapers plasma5-applets-weather-widget
sudo pacman -S --needed --noconfirm plasma-vault plasma5-applets-thermal-monitor kup

# Remove kmix (because it's crap)
sudo pacman -R --noconfirm kde-applications-meta
sudo pacman -R --noconfirm kde-multimedia-meta
sudo pacman -R --noconfirm plasma-meta
sudo pacman -R --noconfirm kmix

# Kvantum
sudo pacman -S --needed --noconfirm kvantum-qt5

# Enable Display Manager
sudo systemctl enable sddm.service

# Sound
#sudo pacman -S --needed --noconfirm pulseaudio pulseaudio-alsa pavucontrol
#sudo pacman -S --needed --noconfirm alsa-utils alsa-plugins alsa-lib alsa-firmware
sudo pacman -S --needed --noconfirm gst-plugins-good gst-plugins-bad gst-plugins-base
sudo pacman -S --needed --noconfirm gst-plugins-ugly gstreamer
#sudo pacman -S --needed --noconfirm pipewire pipewire-alsa pipewire-docs pipewire-jack 
#sudo pacman -S --needed --noconfirm pipewire-pulse gst-plugin-pipewire libpipewire02 

# Fonts from 'normal' repositories
sudo pacman -S --needed --noconfirm noto-fonts noto-fonts-emoji
sudo pacman -S --needed --noconfirm adobe-source-code-pro-fonts

# Apps from standard repos
sudo pacman -S --needed --noconfirm keepassxc kid3-qt samba conky veracrypt
sudo pacman -S --needed --noconfirm file-roller evince calibre gnome-disk-utility
sudo pacman -S --needed --noconfirm uget gparted vlc ttf-liberation hunspell-en_us
sudo pacman -S --needed --noconfirm neofetch phonon-qt5-vlc soundconverter
sudo pacman -S --needed --noconfirm cairo-dock cairo-dock-plug-ins pitivi
sudo pacman -S --needed --noconfirm asunder vorbis-tools libogg lib32-libogg
sudo pacman -S --needed --noconfirm liboggz youtube-dl reflector handbrake
sudo pacman -S --needed --noconfirm clementine firefox audacity ntfs-3g qt5ct
sudo pacman -S --needed --noconfirm steam steam-native-runtime p7zip tlp
sudo pacman -S --needed --noconfirm grub-customizer os-prober flameshot
sudo pacman -S --needed --noconfirm variety meld gimp inkscape openshot
sudo pacman -S --needed --noconfirm shotwell simplescreenrecorder lolcat
sudo pacman -S --needed --noconfirm rhythmbox cryfs adapta-gtk-theme geany geany-plugins
sudo pacman -S --needed --noconfirm soundconverter smb4k freerdp vinagre remmina

# System Utilities
sudo pacman -S --needed --noconfirm curl dconf-editor ffmpegthumbnailer
sudo pacman -S --needed --noconfirm dmidecode hardinfo htop mlocate
sudo pacman -S --needed --noconfirm hddtemp lm_sensors lsb-release f2fs-tools
sudo pacman -S --needed --noconfirm net-tools numlockx simple-scan grsync
sudo pacman -S --needed --noconfirm scrot sysstat tumbler vnstat wmctrl
sudo pacman -S --needed --noconfirm unclutter putty whois openssh dialog

# Install OpenVPN Components
sudo pacman -S --needed --noconfirm openvpn resolvconf networkmanager-openvpn

# Install Libre Office or Free Office
sudo pacman -S --needed --noconfirm libreoffice-fresh
#trizen -S freeoffice --noconfirm

# Install all needed packages for full QEMU/KVM support
#sudo pacman -S --needed --noconfirm iptables-nft dnsmasq vde2 virt-manager
#sudo pacman -S --needed --noconfirm qemu libvirt edk2-ovmf qemu-arch-extra
#sudo pacman -S --needed --noconfirm qemu-block-iscsi qemu-guest-agent

# Enable libnvirt daemon
#sudo systemctl enable libvirtd.service

# Optional - install xdg-user-dirs and update my home directory.
#This creates the usual folders, Documents, Pictures, etc.
sudo pacman -S --needed --noconfirm xdg-user-dirs
xdg-user-dirs-update --force

# Printing Support
sudo pacman -S --needed --noconfirm cups cups-pdf
sudo pacman -S --needed --noconfirm foomatic-db-engine
sudo pacman -S --needed --noconfirm foomatic-db foomatic-db-ppds
sudo pacman -S --needed --noconfirm foomatic-db-nonfree-ppds
sudo pacman -S --needed --noconfirm foomatic-db-gutenprint-ppds
sudo pacman -S --needed --noconfirm ghostscript gsfonts gutenprint
sudo pacman -S --needed --noconfirm gtk3-print-backends
sudo pacman -S --needed --noconfirm libcups
sudo pacman -S --needed --noconfirm hplip
sudo pacman -S --needed --noconfirm system-config-printer
sudo systemctl enable cups.service

# Network Discovery
sudo pacman -S --needed --noconfirm avahi
sudo systemctl enable avahi-daemon.service
sudo systemctl start avahi-daemon.service

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/Buuf-Plasma-1.7.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf ../tarballs/arch-logo.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/smb.conf.tar.gz -C /etc/samba/ --overwrite
sudo tar xzf ../tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf ../tarballs/share.tar.gz -C /usr/share/ --overwrite

# Copy over GRUB theme
sudo tar xzf ../tarballs/arch-silence.tar.gz -C /boot/grub/themes/ --overwrite

# Copy over SDDM theme
sudo tar xzf ../tarballs/archlinux-sddm-theme.tar.gz -C /usr/share/sddm/themes/ --overwrite

# Copy SDDM config
sudo tar xzf ../tarballs/sddm-conf.tar.gz -C /etc/ --overwrite

# Additional hardware decoding for Intel chips.
#footrizen -S --noconfirm intel-hybrid-codec-driver-gcc10

# Add screenfetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

# Fix for Plasma permissions error
sudo chown -R palanthis:palanthis /home/palanthis
sudo chmod 770 /home/palanthis

echo " "
echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."
sudo reboot
