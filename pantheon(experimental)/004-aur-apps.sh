#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Apps from AUR
yay -S --noconfirm --needed gitkraken
yay -S --noconfirm --needed rar
yay -S --noconfirm --needed youtube-dl-gui-git
yay -S --noconfirm --needed tlpui-git
yay -S --noconfirm --needed systemd-kcm
yay -S --noconfirm --needed makemkv
